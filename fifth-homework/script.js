const wallet = {
  userName: prompt("What's your name?"),
  bitcoin: {
    name: 'Bitcoin',
    logo: '<img src="https://s2.coinmarketcap.com/static/img/coins/64x64/1.png" alt="">',
    coins: 3,
    price: 1168037.66
  },
  ethereum: {
    name: 'Ethereum',
    logo: '<img src="https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png" alt="">',
    coins: 2,
    price: 85299.41
  },
  stellar: {
    name: 'Stellar',
    logo: '<img src="https://s2.coinmarketcap.com/static/img/coins/64x64/512.png" alt="">',
    coins: 1,
    price: 5.45
  },
  showInfo: function(coinName) {
    document.write('<div>Доброго дня, ' + wallet.userName + '! На Вашому рахунку ' 
    + wallet[coinName].name + wallet[coinName].logo + ' залишилось ' + wallet[coinName].coins 
    + ' монет, якщо Ви продасте їх сьогодні, то отримаєте ' 
    + wallet[coinName].coins*wallet[coinName].price + ' гривень.</div>')
  }
}

wallet.showInfo(prompt('Enter coin name:').toLowerCase());
