const arr = ['January', 'February', 'March', 'April', 'May', 'June', 
'July', 'August', 'September', 'October', 'November', 'December'];

document.write('<span>Масив, дані якого розділені спецсимволом &#10084</span>');
let res = arr.join('&#10084');
document.write('<div>' + res + '</div>');

document.write('<hr>');

document.write('<span>Масив, кожен елемент якого виводиться з нового рядка</span>');
let newStr = arr.join('<br>');
document.write('<div>' + newStr + '</div>');

document.write('<hr>');

document.write('<span>Вивід елементів масиву з нового рядка за допомогою циклу "for"</span>');

for (let i = 0; i < arr.length; i++) {
    document.write('<div>' + arr[i] + '</div>');
}
